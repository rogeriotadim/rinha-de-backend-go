FROM golang:1.21 as builder

WORKDIR /app

# Copy Go module files
COPY ./src/go.* ./

# Download dependencies
RUN go mod download

# Copy source files
COPY ./src/cmd ./cmd
COPY ./src/internal ./internal
COPY ./src/pkg ./pkg
COPY ./src/.env ./

# Build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -v -o ./bin/rinha ./cmd/main.go

FROM alpine:3.18.4

RUN apk update \
        && apk upgrade \
         && apk --update add --no-cache ca-certificates openssl dumb-init

EXPOSE 3000

# Copy files from builder stage
COPY --from=builder /app/bin/rinha ./app/
COPY --from=builder /app/.env ./app/.env

# Increase GC percentage and limit the number of OS threads
ENV GOGC 1000
ENV GOMAXPROCS 3

# workaround pid==1
ENTRYPOINT ["/usr/bin/dumb-init", "--"]

# Run binary
CMD ["/app/rinha"]
