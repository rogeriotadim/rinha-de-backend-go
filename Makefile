dev-exec:
	docker exec -it app01 /app/rinha 

build: clean
	# docker run CGO_ENABLED=0 go build -pgo=./cmd/cpu.pprof -v -o ./bin/rinha ./cmd/rinha.go
	docker run -it --rm --name build -e CGO_ENABLED=0 -w="/app" -v $(pwd):"/app" go-rt-dev:1.21.4 make build

clean:
	rm -rf ./src/bin/rinha

docker-build:
	docker build --no-cache -t rinha:0.9.0 .

docker-push:
	docker buildx build --push --platform linux/arm/v7,linux/arm64/v8,linux/amd64 --tag  .

down:
	docker compose -f docker-compose-dev.yml down -v --remove-orphans
	docker compose -f docker-compose.yml down -v --remove-orphans

up: down
	docker compose -f docker-compose-dev.yml up

prod-up: down
	docker compose -f docker-compose.yml up
	# sleep 3
	# docker compose ps
