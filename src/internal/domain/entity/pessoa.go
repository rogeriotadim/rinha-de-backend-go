package entity

import (
	"errors"
	"time"

	"github.com/rogeriotadim/rinha-de-backend-go/pkg"
)

var (
	ErrInvalidApelido          = errors.New("apelido inválido")
	ErrInvalidNome             = errors.New("nome inválido")
	ErrInvalidNascimento       = errors.New("nascimento inválido")
	ErrInvalidStack            = errors.New("stack inválida")
	ErrInvalidSintaxApelido    = errors.New("sintaxe apelido inválida")
	ErrInvalidSintaxNome       = errors.New("sintaxe nome inválida")
	ErrInvalidSintaxNascimento = errors.New("sintaxe nascimento inválida")
	ErrInvalidSintaxStack      = errors.New("sintaxe stack inválida")
	ErrDoubleApelido           = errors.New("apelido duplicado")
	ErrPessoaNotFound          = errors.New("pessoa não encontrada")
)

type Pessoa struct {
	ID         string
	Apelido    string
	Nome       string
	Nascimento string
	Stack      []string
}

func NewPessoa(apelido, nome, nascimento string, stack []string) (pessoa *Pessoa, err error) {
	err = validate(apelido, nome, nascimento, stack)
	if err != nil {
		return nil, err
	}

	return &Pessoa{
		ID:         pkg.NewID().String(),
		Apelido:    apelido,
		Nome:       nome,
		Nascimento: nascimento,
		Stack:      stack,
	}, nil
}

func validate(apelido, nome, nascimento string, stack []string) error {
	if apelido == "" || len(apelido) > 32 {
		return ErrInvalidApelido
	}

	if nome == "" || len(nome) > 100 {
		return ErrInvalidNome
	}

	if len(nascimento) != 10 {
		return ErrInvalidNascimento
	}

	layout := "2006-01-02"
	if _, err := time.Parse(layout, nascimento); err != nil {
		return ErrInvalidNascimento
	}
	if len(stack) > 0 {
		for _, tec := range stack {
			if len(tec) > 32 {
				return ErrInvalidStack
			}
		}
	}
	return nil
}
