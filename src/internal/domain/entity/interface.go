package entity

type PessoaRepositoryInterface interface {
	GetContagem() (int64, error)
	GetPessoaByID(id string) (pessoa *Pessoa, err error)
	GetPessoasByTermo(termo string) (pessoas []*Pessoa, err error)
	CreatePessoa(pessoa *Pessoa) error
}

type PessoaCacheInterface interface {
	Get(key string) (*Pessoa, error)
	Delete(key string)
	Set(*Pessoa) error
}
