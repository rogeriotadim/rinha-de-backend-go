package dto

import (
	"errors"
	"time"
)

var ErrInvalidDto = errors.New("invalid dto")

type PessoaDtoIn struct {
	Apelido    string   `json:"apelido" validate:"required,max=32"`
	Nome       string   `json:"nome" validate:"required,max=100"`
	Nascimento string   `json:"nascimento" validate:"required,datetime=2001-12-31"`
	Stack      []string `json:"stack" validate:"dive,max=32"`
}

func (in *PessoaDtoIn) Validate() error {
	if len(in.Apelido) > 32 {
		return ErrInvalidDto
	}

	if len(in.Nome) > 100 {
		return ErrInvalidDto
	}

	dateLayout := "2006-01-02"
	if _, err := time.Parse(dateLayout, in.Nascimento); err != nil {
		return ErrInvalidDto
	}

	for _, item := range in.Stack {
		if len(item) > 32 {
			return ErrInvalidDto
		}
	}

	return nil
}

type PessoaDtoOut struct {
	Id         string   `json:"id"`
	Apelido    string   `json:"apelido"`
	Nome       string   `json:"nome"`
	Nascimento string   `json:"nascimento"`
	Stack      []string `json:"stack"`
}
