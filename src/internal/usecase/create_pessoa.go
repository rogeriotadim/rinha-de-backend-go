package usecase

import (
	"github.com/rogeriotadim/rinha-de-backend-go/internal/domain/dto"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/domain/entity"
)

type CreatePessoaUseCase struct {
	PessoaRepository entity.PessoaRepositoryInterface
	PessoaCache      entity.PessoaCacheInterface
}

func NewCreatePessoaUseCase(
	pessoaRepository entity.PessoaRepositoryInterface,
	pessoaCache entity.PessoaCacheInterface) *CreatePessoaUseCase {
	return &CreatePessoaUseCase{
		PessoaRepository: pessoaRepository,
		PessoaCache:      pessoaCache,
	}
}

func (gc *CreatePessoaUseCase) CreatePessoa(pessoaIn dto.PessoaDtoIn) (*entity.Pessoa, error) {
	pessoa, err := entity.NewPessoa(pessoaIn.Apelido, pessoaIn.Nome, pessoaIn.Nascimento, pessoaIn.Stack)
	if err != nil {
		return nil, err
	}
	err = gc.PessoaCache.Set(pessoa)
	if err != nil {
		return nil, err
	}
	err = gc.PessoaRepository.CreatePessoa(pessoa)
	if err != nil {
		gc.PessoaCache.Delete(pessoa.ID)
		return nil, err
	}
	return pessoa, nil
}
