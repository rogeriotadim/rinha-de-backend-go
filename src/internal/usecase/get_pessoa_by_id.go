package usecase

import "github.com/rogeriotadim/rinha-de-backend-go/internal/domain/entity"

type GetPessoaByIDUseCase struct {
	PessoaRepository entity.PessoaRepositoryInterface
	PessoaCache      entity.PessoaCacheInterface
}

func NewGetPessoaByIDUseCase(
	pessoaRepository entity.PessoaRepositoryInterface,
	pessoaCache entity.PessoaCacheInterface) *GetPessoaByIDUseCase {
	return &GetPessoaByIDUseCase{
		PessoaRepository: pessoaRepository,
		PessoaCache:      pessoaCache,
	}
}

func (gc *GetPessoaByIDUseCase) GetPessoaByID(pessoaID string) (pessoa *entity.Pessoa, err error) {
	pessoa, err = gc.PessoaCache.Get(pessoaID)
	if err != nil {
		return nil, err
	}
	if pessoa != nil {
		return pessoa, nil
	}
	pessoa, err = gc.PessoaRepository.GetPessoaByID(pessoaID)
	if err != nil {
		return nil, err
	}
	return pessoa, nil
}
