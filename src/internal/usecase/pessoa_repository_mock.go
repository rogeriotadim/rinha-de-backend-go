package usecase

import "github.com/rogeriotadim/rinha-de-backend-go/internal/domain/entity"

type PessoaRepository struct {
}

func NewPessoaRepository() *PessoaRepository {
	return &PessoaRepository{}
}

func (r *PessoaRepository) CreatePessoa(pessoa *entity.Pessoa) error {
	return nil
}

func (r *PessoaRepository) GetPessoaByID(id string) (*entity.Pessoa, error) {
	return &entity.Pessoa{
		ID:         "321",
		Apelido:    "321",
		Nome:       "321",
		Nascimento: "1971-12-25",
		Stack:      []string{"go", "devops"},
	}, nil
}

func (r *PessoaRepository) GetContagem() (int64, error) {
	return 0, nil
}

func (r *PessoaRepository) GetPessoasByTermo(termo string) ([]*entity.Pessoa, error) {

	return nil, nil
}
