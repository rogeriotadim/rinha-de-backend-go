package usecase

import "github.com/rogeriotadim/rinha-de-backend-go/internal/domain/entity"

type GetPessoasByTermoUseCase struct {
	PessoaRepository entity.PessoaRepositoryInterface
}

func NewGetPessoasByTermoUseCase(pessoaRepository entity.PessoaRepositoryInterface) *GetPessoasByTermoUseCase {
	return &GetPessoasByTermoUseCase{
		PessoaRepository: pessoaRepository,
	}
}

func (gc *GetPessoasByTermoUseCase) GetPessoasByTermo(termo string) (pessoas []*entity.Pessoa, err error) {
	pessoas, err = gc.PessoaRepository.GetPessoasByTermo(termo)
	if err != nil {
		return nil, err
	}
	return pessoas, nil
}
