package usecase

import "github.com/rogeriotadim/rinha-de-backend-go/internal/domain/entity"

type GetContagemUseCase struct {
	PessoaRepository entity.PessoaRepositoryInterface
}

func NewGetContagemUseCase(pessoaRepository entity.PessoaRepositoryInterface) *GetContagemUseCase {
	return &GetContagemUseCase{
		PessoaRepository: pessoaRepository,
	}
}

func (gc *GetContagemUseCase) GetContagem() (int64, error) {
	qtd, err := gc.PessoaRepository.GetContagem()
	if err != nil {
		return 0, err
	}
	return qtd, nil
}
