package server

import (
	"errors"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/domain/dto"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/domain/entity"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/infra/cache"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/infra/database"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/usecase"
)

type PessoasHandler struct {
	Repository *database.PessoaRepository
	Cache      *cache.Cache
}

func NewPessoasHandler(repository *database.PessoaRepository, cache *cache.Cache) *PessoasHandler {
	return &PessoasHandler{
		Repository: repository,
		Cache:      cache,
	}
}

func (p *PessoasHandler) CreatePessoa(c *fiber.Ctx) error {
	uc := usecase.NewCreatePessoaUseCase(p.Repository, p.Cache)
	pessoaIn := new(dto.PessoaDtoIn)

	err := c.BodyParser(pessoaIn)
	if err != nil && strings.Contains(err.Error(), "unmarshal") {
		return c.SendStatus(fiber.ErrBadRequest.Code)
	}
	if err != nil {
		return c.SendStatus(fiber.ErrInternalServerError.Code)
	}

	pessoa, err := uc.CreatePessoa(*pessoaIn)
	if errors.Is(err, entity.ErrDoubleApelido) ||
		errors.Is(err, entity.ErrInvalidApelido) ||
		errors.Is(err, entity.ErrInvalidNome) ||
		errors.Is(err, entity.ErrInvalidNascimento) ||
		errors.Is(err, entity.ErrInvalidStack) {
		return c.SendStatus(fiber.ErrUnprocessableEntity.Code)
	}
	if errors.Is(err, entity.ErrInvalidSintaxApelido) ||
		errors.Is(err, entity.ErrInvalidSintaxNome) ||
		errors.Is(err, entity.ErrInvalidSintaxNascimento) ||
		errors.Is(err, entity.ErrInvalidSintaxStack) {
		return c.SendStatus(fiber.ErrBadRequest.Code)
	}

	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}
	c.Set("Location", "/pessoas/"+pessoa.ID)

	return c.SendStatus(fiber.StatusCreated)
}

func (p *PessoasHandler) GetPessoaByID(c *fiber.Ctx) error {
	id := c.Params("id")
	if id == "" {
		return c.SendStatus(fiber.ErrBadRequest.Code)
	}
	uc := usecase.NewGetPessoaByIDUseCase(p.Repository, p.Cache)

	pessoa, err := uc.GetPessoaByID(id)
	if err != nil {
		return c.SendStatus(fiber.ErrInternalServerError.Code)
	}
	if pessoa == nil {
		return c.SendStatus(fiber.ErrNotFound.Code)
	}

	return c.Status(fiber.StatusOK).JSON(pessoa)
	// return c.Status(fiber.StatusNotFound).JSON(fiber.Map{"error": "404"})

}

func (p *PessoasHandler) GetPessoasByTermo(c *fiber.Ctx) error {
	termo := c.Query("t")
	if termo == "" {
		return c.SendStatus(fiber.ErrBadRequest.Code)
	}
	uc := usecase.NewGetPessoasByTermoUseCase(p.Repository)

	pessoas, err := uc.GetPessoasByTermo(termo)
	if err != nil {
		return c.SendStatus(fiber.ErrInternalServerError.Code)
	}
	return c.Status(fiber.StatusOK).JSON(pessoas)
}
