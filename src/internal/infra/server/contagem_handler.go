package server

import (
	"strconv"

	"github.com/gofiber/fiber/v2"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/usecase"
)

type ContagemHandler struct {
	UseCase usecase.GetContagemUseCase
}

func NewContagemHandler(usecase *usecase.GetContagemUseCase) *ContagemHandler {
	return &ContagemHandler{UseCase: *usecase}
}

func (ch *ContagemHandler) ContagemPessoas(c *fiber.Ctx) error {
	qtd, err := ch.UseCase.GetContagem()
	if err != nil {
		c.SendStatus(fiber.StatusInternalServerError)
	}
	return c.Status(fiber.StatusOK).SendString(strconv.FormatInt(qtd, 10))

}
