package server

import (
	"testing"

	"github.com/gofiber/fiber/v2"
)

func TestPessoasHandler_GetPessoaByID(t *testing.T) {
	type args struct {
		c *fiber.Ctx
	}
	tests := []struct {
		name    string
		cp      *PessoasHandler
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cp := &PessoasHandler{}
			if err := cp.GetPessoaByID(tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("PessoasHandler.GetPessoaByID() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
