package server

import (
	"testing"

	"github.com/gofiber/fiber/v2"
)

func TestContagemHandler_ContagemPessoas(t *testing.T) {
	type args struct {
		c *fiber.Ctx
	}
	tests := []struct {
		name    string
		cp      *ContagemHandler
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cp := &ContagemHandler{}
			if err := cp.ContagemPessoas(tt.args.c); (err != nil) != tt.wantErr {
				t.Errorf("ContagemHandler.ContagemPessoas() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
