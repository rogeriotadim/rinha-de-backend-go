package database

import (
	"context"
	"errors"
	"strings"

	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/domain/entity"
)

type PessoaRepository struct {
	Db  *pgxpool.Pool
	Ctx context.Context
}
type pessoaDB struct {
	ID         string
	Apelido    string
	Nome       string
	Nascimento string
	Stack      string
}

func (pes *pessoaDB) hidrateEntity() *entity.Pessoa {
	stack := strings.Split(pes.Stack, ",")
	return &entity.Pessoa{
		ID:         pes.ID,
		Apelido:    pes.Apelido,
		Nome:       pes.Nome,
		Nascimento: pes.Nascimento,
		Stack:      stack}
}

func NewPessoaRepository(ctx context.Context, db *pgxpool.Pool) *PessoaRepository {
	return &PessoaRepository{
		Ctx: ctx,
		Db:  db,
	}
}

func (r *PessoaRepository) CreatePessoa(pessoa *entity.Pessoa) error {
	stack := strings.Join(pessoa.Stack[:], ",")
	_, err := r.Db.Exec(r.Ctx,
		"INSERT INTO PESSOAS (ID, APELIDO, NOME, NASCIMENTO, STACK) VALUES ($1, $2, $3, $4, $5)",
		pessoa.ID, pessoa.Apelido, pessoa.Nome, pessoa.Nascimento, stack)
	if err != nil {
		if strings.Contains(err.Error(), "23505") {
			return entity.ErrDoubleApelido
		}
		return err
	}

	return nil
}

func (r *PessoaRepository) GetContagem() (int64, error) {
	var total int64
	err := r.Db.QueryRow(r.Ctx, "Select count(*) from pessoas").Scan(&total)
	if err != nil {
		return 0, err
	}
	return total, nil
}

func (r *PessoaRepository) GetPessoaByID(id string) (*entity.Pessoa, error) {
	var p pessoaDB
	rows, err := r.Db.Query(r.Ctx, "SELECT id,apelido,nome,nascimento,stack FROM PESSOAS WHERE id=$1;", id)
	if rows == nil {
		return nil, err
	}
	p, err = pgx.CollectOneRow(rows, pgx.RowToStructByPos[pessoaDB])
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	}

	return p.hidrateEntity(), nil
}

func (r *PessoaRepository) GetPessoasByTermo(termo string) ([]*entity.Pessoa, error) {
	termo = strings.ToLower(termo)
	rows, err := r.Db.Query(r.Ctx,
		`SELECT id,apelido,nome,nascimento,stack 
			FROM PESSOAS 
			WHERE busca_trgm like '%' || $1 || '%'
			LIMIT 50;`,
		termo)
	if err != nil {
		return nil, nil
	}
	if rows == nil {
		return nil, nil
	}
	listPessoas, err := pgx.CollectRows[*entity.Pessoa](rows, func(row pgx.CollectableRow) (*entity.Pessoa, error) {
		var pess entity.Pessoa
		values, _ := row.Values()
		id := values[0].(string)
		apelido := values[1].(string)
		nome := values[2].(string)
		nascimento := values[3].(string)
		stack := values[4].(string)
		pess.ID = id
		pess.Apelido = apelido
		pess.Nome = nome
		pess.Nascimento = nascimento
		pess.Stack = strings.Split(stack, ",")

		return &pess, err
	})
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	}

	return listPessoas, nil
}
