package cache

import (
	"context"
	"fmt"
	"time"

	"github.com/go-redis/cache/v9"
	"github.com/redis/go-redis/v9"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/domain/entity"
)

type Cache struct {
	MyCache *cache.Cache
	Ctx     context.Context
}

func NewCache(ctx context.Context, host, port string) *Cache {
	address := fmt.Sprintf(
		"%s:%s", host, port,
	)
	ring := redis.NewRing(&redis.RingOptions{
		Addrs: map[string]string{
			"server1": address,
		},
	})

	mycache := cache.New(&cache.Options{
		Redis:      ring,
		LocalCache: cache.NewTinyLFU(1000, time.Minute*3),
	})

	return &Cache{
		MyCache: mycache,
		Ctx:     ctx,
	}
}

func (c *Cache) Get(idPessoa string) (*entity.Pessoa, error) {
	var pessoa *entity.Pessoa
	err := c.MyCache.Get(c.Ctx, idPessoa, &pessoa)
	if err != nil && err.Error() != "cache: key is missing" {
		return nil, err
	}
	return pessoa, nil
}

func (c *Cache) Set(pessoa *entity.Pessoa) error {

	err := c.MyCache.Set(&cache.Item{
		Ctx:   c.Ctx,
		Key:   pessoa.ID,
		Value: pessoa,
		TTL:   time.Minute * 3,
	})
	return err
}

func (c *Cache) Delete(idPessoa string) {
	_ = c.MyCache.Delete(c.Ctx, idPessoa)
}

func NewClient(host, port string, db int) (*redis.Client, error) {
	address := fmt.Sprintf(
		"redis://%s:%s/%d", host, port, db)
	opt, err := redis.ParseURL(address)
	if err != nil {
		return nil, err
	}
	client := redis.NewClient(opt)

	return client, nil

}
