package usecase

import (
	"context"
	"testing"

	"github.com/rogeriotadim/rinha-de-backend-go/cmd/configs"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/infra/database"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/usecase"
	"github.com/stretchr/testify/assert"
)

func TestNewGetContagemUseCaseIntegration(t *testing.T) {
	ctx := context.Background()
	configs, err := configs.LoadConfig("/app")
	assert.Nil(t, err)
	// connStr := fmt.Sprintf("user=%s password=%s host=%s port=%s dbname=%s sslmode=disable", configs.DBUser, configs.DBPassword, configs.DBHost, configs.DBPort, configs.DBName)
	// db, err := sql.Open(configs.DBDriver, connStr)
	pool, err := database.NewDatabasePool(configs, ctx)
	assert.Nil(t, err)
	defer pool.Close()

	repo := database.NewPessoaRepository(context.Background(), pool)
	uc := usecase.NewGetContagemUseCase(repo)
	assert.NotNil(t, uc)
	total, _ := uc.PessoaRepository.GetContagem()
	assert.GreaterOrEqual(t, total, int64(0))
}
