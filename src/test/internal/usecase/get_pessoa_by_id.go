package usecase

import (
	"context"
	"testing"

	"github.com/rogeriotadim/rinha-de-backend-go/cmd/configs"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/infra/cache"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/infra/database"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/usecase"
	"github.com/stretchr/testify/assert"
)

func TestNewGetPessoasByIdUseCaseIntegration(t *testing.T) {
	ctx := context.Background()
	configs, err := configs.LoadConfig("/app")
	assert.Nil(t, err)
	pool, err := database.NewDatabasePool(configs, ctx)
	assert.Nil(t, err)

	repo := database.NewPessoaRepository(context.Background(), pool)
	cachePessoas := cache.NewCache(ctx, configs.RedisHost, configs.RedisPort)

	uc := usecase.NewGetPessoaByIDUseCase(repo, cachePessoas)
	assert.NotNil(t, uc)
	id := "123-456"
	pessoa, err := uc.PessoaRepository.GetPessoaByID(id)
	assert.Nil(t, err)
	assert.GreaterOrEqual(t, id, pessoa.ID)

	pool.Close()
}
