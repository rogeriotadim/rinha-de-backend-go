package usecase

import (
	"context"
	"testing"

	"github.com/rogeriotadim/rinha-de-backend-go/cmd/configs"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/infra/database"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/usecase"
	"github.com/stretchr/testify/assert"
)

func TestNewGetPessoasByTermoUseCaseIntegration(t *testing.T) {
	ctx := context.Background()
	configs, err := configs.LoadConfig("/app")
	assert.Nil(t, err)
	pool, err := database.NewDatabasePool(configs, ctx)
	assert.Nil(t, err)
	defer pool.Close()

	repo := database.NewPessoaRepository(context.Background(), pool)
	uc := usecase.NewGetPessoasByTermoUseCase(repo)
	assert.NotNil(t, uc)
	pessoas, _ := uc.PessoaRepository.GetPessoasByTermo("go")
	assert.GreaterOrEqual(t, len(pessoas), 0)
}
