package usecase

import (
	"context"
	"testing"

	"github.com/rogeriotadim/rinha-de-backend-go/cmd/configs"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/domain/dto"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/infra/cache"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/infra/database"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/usecase"
	"github.com/stretchr/testify/assert"
)

func TestCreatePessoaUseCaseIntegration(t *testing.T) {
	ctx := context.Background()
	configs, err := configs.LoadConfig("/app")
	assert.Nil(t, err)

	pool, err := database.NewDatabasePool(configs, ctx)
	assert.Nil(t, err)
	defer pool.Close()

	repo := database.NewPessoaRepository(context.Background(), pool)
	cache := cache.NewCache(ctx, "localhost", "6379")
	uc := usecase.NewCreatePessoaUseCase(repo, cache)
	assert.NotNil(t, uc)

	pessoaIn := dto.PessoaDtoIn{
		Apelido:    "tadim",
		Nome:       "rogerio",
		Nascimento: "1971-12-25",
		Stack:      []string{"go", "devops"},
	}
	pessoa, err := uc.CreatePessoa(pessoaIn)
	assert.Nil(t, err)
	assert.Equal(t, pessoaIn.Apelido, pessoa.Apelido)

}
