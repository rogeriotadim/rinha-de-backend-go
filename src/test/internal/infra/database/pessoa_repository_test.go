package database

import (
	"context"
	"math/rand"
	"strconv"
	"testing"

	"github.com/rogeriotadim/rinha-de-backend-go/cmd/configs"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/domain/entity"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/infra/database"
	"github.com/stretchr/testify/assert"
)

// var ctx context.Context = context.Background()

func TestCreatePessoaIntegration(t *testing.T) {
	ctx := context.Background()
	configs, err := configs.LoadConfig("/app")
	assert.Nil(t, err)
	dbPool, err := database.NewDatabasePool(configs, ctx)
	assert.Nil(t, err)

	repository := database.NewPessoaRepository(ctx, dbPool)

	stack := []string{"Go", "Kubernetes", "AWS"}
	pessoa, err := entity.NewPessoa("Doe "+strconv.Itoa(rand.Int()), "John", "1971-12-25", stack)
	assert.Nil(t, err)
	err = repository.CreatePessoa(pessoa)
	assert.Nil(t, err)
	dbPool.Close()
}

func TestContagemPessoasIntegration(t *testing.T) {
	ctx := context.Background()
	configs, err := configs.LoadConfig("/app")
	assert.Nil(t, err)
	dbPool, err := database.NewDatabasePool(configs, ctx)
	assert.Nil(t, err)

	repository := database.NewPessoaRepository(ctx, dbPool)

	qtd, err := repository.GetContagem()
	assert.Nil(t, err)
	assert.GreaterOrEqual(t, qtd, int64(0))
	dbPool.Close()
}

func TestFindPessoaIntegration(t *testing.T) {
	ctx := context.Background()
	configs, err := configs.LoadConfig("/app")
	assert.Nil(t, err)
	dbPool, err := database.NewDatabasePool(configs, ctx)
	assert.Nil(t, err)

	repository := database.NewPessoaRepository(ctx, dbPool)

	id := "123-456"
	pessoa, err := repository.GetPessoaByID(id)
	assert.Nil(t, err)
	assert.NotNil(t, pessoa)
	assert.Equal(t, id, pessoa.ID)
	dbPool.Close()
}

func TestFindPessoasIntegration(t *testing.T) {
	ctx := context.Background()
	configs, err := configs.LoadConfig("/app")
	assert.Nil(t, err)
	dbPool, err := database.NewDatabasePool(configs, ctx)
	assert.Nil(t, err)

	repository := database.NewPessoaRepository(ctx, dbPool)

	termo := "go"
	pessoas, err := repository.GetPessoasByTermo(termo)
	assert.Nil(t, err)
	assert.NotNil(t, pessoas)
	assert.GreaterOrEqual(t, len(pessoas), 0)
	dbPool.Close()
}
