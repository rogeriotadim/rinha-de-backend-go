package cache

import (
	"context"
	"testing"

	"github.com/rogeriotadim/rinha-de-backend-go/cmd/configs"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/domain/entity"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/infra/cache"
	"github.com/stretchr/testify/assert"
)

var (
	host   string = "redis"
	port   string = "6379"
	pessoa *entity.Pessoa
	ctx    context.Context = context.Background()
)

func TestCacheIntegration(t *testing.T) {
	configs, err := configs.LoadConfig("/app")
	assert.Nil(t, err)
	host = configs.RedisHost
	port = configs.RedisPort

	mycache := cache.NewCache(ctx, host, port)
	assert.NotNil(t, mycache)
	pessoa = newPessoa()

	err = mycache.Set(pessoa)
	assert.Nil(t, err)

	pessoaTarget, err := mycache.Get(pessoa.ID)
	assert.Nil(t, err)
	assert.NotNil(t, pessoaTarget)
	assert.Equal(t, pessoa.ID, pessoaTarget.ID)

	mycache.Delete(pessoaTarget.ID)

}

func newPessoa() *entity.Pessoa {
	pessoa, _ := entity.NewPessoa("apelido", "nome", "1971-12-25", []string{"123", "456"})

	return pessoa

}
