package main

import (
	"context"
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/rogeriotadim/rinha-de-backend-go/cmd/configs"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/infra/cache"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/infra/database"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/infra/server"
	"github.com/rogeriotadim/rinha-de-backend-go/internal/usecase"

	// postgres
	_ "github.com/lib/pq"
)

func main() {

	ctx := context.Background()
	configs, err := configs.LoadConfig("/app")
	if err != nil {
		log.Fatalf("failed to open config: %v", err)
	}
	dbPool, err := database.NewDatabasePool(configs, ctx)
	if err != nil {
		log.Fatalf("failed to open database: %v", err)
	}
	defer dbPool.Close()

	webServerConfig := fiber.Config{
		ServerHeader: "rinha de backend", // add custom server header
		Prefork:      configs.Prefork,
		AppName:      "rinha de backend - go",
	}
	app := fiber.New(webServerConfig)

	repoPessoas := database.NewPessoaRepository(ctx, dbPool)
	cachePessoas := cache.NewCache(ctx, configs.RedisHost, configs.RedisPort)

	ucContagem := usecase.NewGetContagemUseCase(repoPessoas)
	ch := server.NewContagemHandler(ucContagem)
	app.Get("/contagem-pessoas", ch.ContagemPessoas)

	pessoaHandler := server.NewPessoasHandler(repoPessoas, cachePessoas)
	app.Post("/pessoas", pessoaHandler.CreatePessoa)
	app.Get("/pessoas/:id", pessoaHandler.GetPessoaByID)
	app.Get("/pessoas/", pessoaHandler.GetPessoasByTermo)

	// app.Get("/metrics", monitor.New())

	println("Vai CORINTHIANS!!!")

	log.Fatalf("failed to start server: %v", app.Listen(fmt.Sprintf(":%s", configs.WebServerPort)))

}
